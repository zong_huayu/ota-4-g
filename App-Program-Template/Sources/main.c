/*---------------------------------------------------------*/
/************************************************************/
/* 程序说明：
 * 1.该程序为远程程序更新的模板，请不要删除该文件中原有的
 *   程序; 可以在该模板中添加自己的程序
 * 2.程序默认的链接脚本为：App-A.prm, 如果要远程更新程序，
 * 请修改链接脚本为App-B.prm，下次要再进行远程更新，需要
 * 将链接脚本改回App-A.prm，以此类推，每次要进行远程程序更
 * 新前，请重新指定一个链接脚本。
 * 3.更改链接脚本的方法：
 *      Edit-->Standard Settings-->Linker for HC12,选中
 *  Use Custom PRM file单选框，然后在当前程序目录-->prm文件夹
 *  下选择对应的链接脚本。
 * 4.保证时钟为32MHz
 */
/************************************************************/
/*---------------------------------------------------------*/
#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */
#include "CAN.h" 
#define BUS_CLOCK		        32000000	    //单片机总线频率32MHz
#define OSC_CLOCK		        16000000      //单片机总线频率16MHz

#define READ_U8_FAR(addr)       ((unsigned char)(*(volatile unsigned char *__far)(addr)))

#define CANID_OnlineCheck_Assist   0x00003e81  
#define CANID_OnlineCheck          0x000000a1

#define data_len_TX          8           //发送数据长度
struct can_msg msg_send, msg_get;
uint g_engine_non_start = 1;

byte data = 0x01;

/*************************************************************/
/*                      初始化锁相环                         */
/*************************************************************/
void INIT_PLL(void) 
{
    CLKSEL &= 0x7f;       //set OSCCLK as sysclk
    PLLCTL &= 0x8F;       //Disable PLL circuit
    CRGINT &= 0xDF;
    
    #if(BUS_CLOCK == 40000000) 
      SYNR = 0x44;
    #elif(BUS_CLOCK == 32000000)
      SYNR = 0x43;     
    #elif(BUS_CLOCK == 24000000)
      SYNR = 0x42;
    #endif 

    REFDV = 0x81;         //PLLCLK=2×OSCCLK×(SYNDIV+1)/(REFDIV+1)＝64MHz ,fbus=32M
    PLLCTL =PLLCTL|0x70;  //Enable PLL circuit
    asm NOP;
    asm NOP;
    while(!(CRGFLG&0x08)); //PLLCLK is Locked already
    CLKSEL |= 0x80;        //set PLLCLK as sysclk
}


void force_reset(void)
{
  uint illegal_instruction = 0x9e10;  //找一条不可能出现的指令编码
  ((void (*)(void))illegal_instruction)(); //execute illegal instruction will result a HW reset 
}

void delay(void)
{
  int i, j;
  
  for(i=100; i>0; i--)
    for(j=0; j<5000; j++)
      ;
}

/*************************************************************/
/*                      中断接收函数（J1939）                */
/*************************************************************/
#pragma CODE_SEG __NEAR_SEG NON_BANKED
void interrupt CAN_receive(void) 
{
  if(MSCAN0GetMsg(&msg_get)) 
  {
    // 接收新信息
    if( (msg_get.id == CANID_OnlineCheck_Assist || msg_get.id == CANID_OnlineCheck) 
          && (!msg_get.RTR)) 
    {
      if(g_engine_non_start) 
      {
        //reset, enter bootloader
        force_reset(); 
         
      }
    }
  }
  else 
  {
    for(;;);
  }
}


#pragma CODE_SEG DEFAULT

/*************************************************************/
/*                          主函数                           */
/*************************************************************/
void main(void) 
{
	IVBR = READ_U8_FAR(0x100308); //修改中断基址寄存器
	DisableInterrupts;
	INIT_PLL();
  INIT_CAN0();
  
  DDRB = 0xff;
  PORTB = ~data;
	EnableInterrupts;

  for(;;) 
  {
    delay();
    //data=data<<1;         //左移一位
    if(data==0)
        data=0x01;
    PORTB = ~data;
  } 

}
